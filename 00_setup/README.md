<h1>Basic setup for primer</h1>

<h2>Install the Cmd2 Python3 Module</h2>

```bash
sudo pip3 install cmd2
```

<h2>Python2 is EOL for Cmd2 so it is recommended that you use Python3</h2>