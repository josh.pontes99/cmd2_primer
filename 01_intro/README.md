<h1>Intro to Cmd2</h1>

  

<h2>What is Cmd2?</h2>

  

<p1>Cmd2 is a Python module used as a framework to create command line interpreters</p1>

  

<h2>Why use Cmd2?</h2>

<p1>Cmd2 is build on the standard Python libary Cmd, this module allows you to create user friendly command line apps. Cmd2 comes with some more advanced features installed by default like tab completion and a vast command argument system that we will go over later.</p1>

<h2>Cmd vs Cmd2</h2>
<p1>Cmd2 is built off of the Python standard library Cmd, and adds some amazing features such as:</p1>

 - Tab completion
 - Parse commands with argparse
 - Macros/aliases
 - Command history

<h2>Running the command line app</h2>

  <p1>I included a base Cmd2 app for you to run to see what it looks like, follow along with the comments in the apps code to understand each part. We will go over adding functionality later</p1>
  

```bash

python3 first_app.py

```

  

<p1>Its that easy, the cmdloop method thats being called automatically starts the app so all you need to do is run the python file</p1>
