#First we need to import the cmd2 module
from cmd2 import Cmd

#Then we need to create a class that will be a subclass of the Cmd object
#for now we can just pass inside this class, nothing else is needed for a base app
class interpreter(Cmd):
    pass

#we create an instance of our interperter
app = interpreter()

#then we call the cmdloop method to actually start the command line interpreter 
app.cmdloop()