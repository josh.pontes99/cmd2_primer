<h1>Adding functionality to your app</h1>

<h2>Do functions in Cmd2</h2>
<p1>Cmd2 uses something called "do functions", these functions are what Cmd2 uses to determine what commands should be added into the app</p1>

<h2>Defining do functions</h2>
<p1>in Cmd2 all functions are created inside of our class that we create as a subclass of the Cmd2 cmd object, but "do functions" are defined by adding "do_" to the beginning of your function</p1>
<pre><code>
from Cmd2 import cmd
class interpreter(cmd):
	def  do_say_hello(self, args):
		print(f"Hello, {args}")
 </code></pre>
 <p1>Here we create a function called "do_say_hello" so Cmd2 looks at that and creates a command called "say_hello". Since this is a method inside a class we have to pass the "self" argument and Cmd2 expects the "args" argument which is anything on the line after the command is called. For example our function above would have the following result inside the app.</p1>

```bash
(cmd) say_hello John
Hello, John
(cmd)
```

<h2>Extra Customization</h2>
<h4>Custom Prompts</h4>
<p1>When running the app the text "(cmd)" show up on each line, this is know as the prompt for the app and can easily be changed by adding the following line to your class</p1>
<pre><code>
prompt = "custom prompt"
</code></pre>

<h4>Custom Intro</h4>
<p1>There is also a option to add a opening text when your app is first started that will print to the user, this is changed the same way as the prompt but using the intro variable</p1>
<pre><code>
intro = "custom intro"
</code></pre>