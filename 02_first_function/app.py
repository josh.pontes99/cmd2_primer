from cmd2 import Cmd

class interpreter(Cmd):
    #our custom function that when given a name says hi to them
    def do_say_hello(self, args):
        print(f"Hello, {args}")


app = interpreter()

app.cmdloop()