<h1>Managing arguments in Cmd2</h1>

<h2>Argparse integration with Cmd2</h2>
<p1>One of the advantages of using Cmd2 vs Cmd is how arguments are handled in Cmd2. Normally you would have to pass 1 variable that represents all the text on the line after the command, as demonstrated in the last section. This method is extremely frustrating to work with as you have to figure out how to manually parse through the data. Cmd2 addresses this problem by creating a helper function called "with_argparser"  that uses the python standard library "argparse" to handle arguments. This wont be an argparse walk through, we will only be going over its integration with Cmd2. However argparse if fairly simple and well documented so if you have any questions the documentation is a good place to look.</p1>

<p1>First we need to import the "with_argparser" helper function and argparse itself</p1>
<pre><code>
from cmd2 import Cmd, with_argparser
import argparse
</code></pre>

<p1>Then we need to create our parser</p1>
<pre><code>
parser = argparse.ArgumentParser()
</code></pre>

<p1>Our parser now needs some arguments, we will add a name argument to build off of the last exercise</p1>

<pre><code>
parser.add_argument("-name", help="name to say hello to", required=True)
</code></pre>
<p1>Now we need to add the parser to our original function with the "with_argparser" helper function</p1>
<pre><code>
class interpreter(cmd):

	#the helper function using the parser
	@with_argparser(parser)
	def do_say_hello(self, args):
		#notice how args becomes args.name
		#this is because its an argparse object now
		print(f"Hello, {args.name}")

</code></pre> 

<h2>Using our new parser</h2>
<p1>Now we need to call our command using the new parser. This is super simple, since we created an argument called "-name" we use that same syntax to pass the name variable to our function</p1>

```bash
(Cmd) say_hello -name John
Hello, John
(Cmd) 
```

