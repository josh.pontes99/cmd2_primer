from cmd2 import Cmd, with_argparser
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-name', help="name to say hello to", required=True)

class interpreter(Cmd):
    
    @with_argparser(parser)
    def do_say_hello(self, args):
        print(f"Hello, {args.name}")


app = interpreter()

app.cmdloop()