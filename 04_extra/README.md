<h2>Plugin System</h2>

<h3>What is this?</h3>
<p1>This is completely extra and is not included in the default version of Cmd2, this is about a plugin system that i created for Cmd2 that allows for modular creation of commands</p1>

<h3>Why use a plugin system?</h3>
<p1>My reason for wanting to create a plugin system is for organization and increased productivity when working in teams. Since all the commands in Cmd2 are created under the same class things can get a little cluttered and confusing in bigger projects. Also since normally you would be creating the commands all in the same file it can be easy to create conflicts in the code with others who might be working on a similar feature, this is also solved with a plugin system as a person working on one feature would have their own file that gets imported at run time.</p1>

<h3>How to use the plugin system</h3>
<h4>Installation</h4>
<p1>I've included the folder called "plugin_manager" this is where our manager and plugins will live we will go over creating plugins later, for now we will focus on installation. **Before we write any code make sure your app.py is in the same folder with the "plugin_manager" folder**</p1>

<p1>First we need to import our manager along with Cmd2 with the following import statement</p1>

<pre><code>
from plugin_manager.manager import plugin_manager
from cmd2 import Cmd
</code></pre>

<p1>Next you create your class as usual, for now we will just pass inside the class cause we will be adding our functions later</p1>
<pre><code>
class  interpreter(Cmd):
	pass
</code></pre>

<p1>Now that we have our class created we need to create an instance our the manager class we imported and pass it the class we created</p1>

<pre><code>
manager =  plugin_manager(interpreter)
</code></pre>

<p1>That's it as far as setup goes after that you can initialize your app like normal</p1>
<pre><code>
app =  interpreter()
app.cmdloop()
</code></pre>

<h4>Creating plugins</h4>
<p1>In the "plugin_manager" folder you will notice there is another folder called "plugins", this is where all of our plugins will live. Here you can create any python file and it will automatically look through the folder for functions to import.</p1>

<p1>After creating a file in the "plugins" folder all you need to do is import the plugin manager again and create a subclass of the "plugin_manager" class as follows</p1>

<pre><code>
from plugin_manager.manager import plugin_manager

class  test(plugin_manager):
	pass
</code></pre>

<p1>Now that we have this class created any "do" functions that we create in that class will automatically be imported into your main app</p1>

<p1>For example the following code would add a command called "hello" that when called just prints "Hello World" to the user</p1>
<pre><code>
class  test(plugin_manager):
	def do_hello(self, args):
		print("Hello World")
</code></pre>

<h4>Using Argparse with plugins</h4>
<p1>working with argparse in the plugin system works exactly the same as before,however if you want to use argparse inside the plugins make sure you import the "with_argparser" helper function from Cmd2 and argparse into the plugin file itself.</p1>

<p1>Ive included 2 plugins in the "plugin" folder for you to reference, one is using argparse to echo back a word to the user, while the other is just a simple hello world command.</p1>





