#!/usr/bin/python3

from cmd2 import Cmd, with_argparser
import argparse
from plugin_manager.manager import plugin_manager

class interpreter(Cmd):
    intro = "welcome to my example plugin system"
    prompt = "> "

manager = plugin_manager(interpreter)

app = interpreter()
app.cmdloop()