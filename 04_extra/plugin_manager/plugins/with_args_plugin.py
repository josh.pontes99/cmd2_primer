from plugin_manager.manager import plugin_manager
from cmd2 import with_argparser
import argparse

argparser = argparse.ArgumentParser()
argparser.add_argument('-word', help="this is a test", required=True)


class test(plugin_manager):
    
    @with_argparser(argparser)
    def do_echo(self, args):
        print(args.word)
